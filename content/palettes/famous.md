---
title: "Famous"
date: 2021-10-13T18:44:37-03:00
draft: false
---
---

![facebook]({{< resource url="/images/facebook.svg" >}})

### Facebook

This is the color palette used by Facebook on their website and app.

#### Hex Codes:

1. #3B5998
2. #8B9DC3
3. #DFE3EE
4. #F7F7F7
5. #FFFFFF
---

![google]({{< resource url="/images/google.svg" >}})

### Google

This is the color palette used by Google on their logo and apps.
#### Hex Codes:

1. #008744
2. #0057E7
3. #D62D20
4. #FFA700
5. #FFFFFF
---

![spotify]({{< resource url="/images/spotify.svg" >}})

### Spotify

This is the color palette used by Spotify on their website and app.
#### Hex Codes:

1. #1DB954
2. #212121
3. #121212
4. #535353
5. #B3B3B3
---
