---
title: "Dark"
date: 2021-10-13T18:44:08-03:00
draft: false
---
---
![nord]({{< resource url="/images/nord.svg" >}})

### Nord 

Created for the clean and uncluttered design pattern to achieve a optimal focus and readability for code syntax highlighting and UI components.
#### Hex Codes:

1. #2E3440
2. #3B4252
3. #434C5E
4. #4C566A
5. #526076
---
![onedark]({{< resource url="/images/onedark.svg" >}})

### One Dark

A dark Vim/Neovim color scheme for the GUI and 16/256/true-color terminals, based on FlatColor, with colors inspired by the excellent One Dark syntax theme for the Atom text editor.
#### Hex Codes:

1. #282C34
2. #E06C75
3. #98C378
4. #E5C07B
5. #61AFFEF
---

![dracula]({{< resource url="/images/dracula.svg" >}})

### Dracula

Dracula is a project created by Zeno Rocha with the help of many awesome contributors and maintainers.
#### Hex Codes:

1. #282A36
2. #FF5555
3. #50FA7B
4. #F1FA8C
5. #BD93F9
---
