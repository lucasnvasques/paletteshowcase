---
title: "Light"
date: 2021-10-13T18:44:17-03:00
draft: false
---
---

![metroui]({{< resource url="/images/metroui.svg" >}})

### MetroUI

This is a light material theme for modern projects.

#### Hex Codes:

1. #E1F7D5
2. #FFBDBD
3. #C9C9FF
4. #FFFFFF
5. #F1CBFF
---

![pastel]({{< resource url="/images/pastel.svg" >}})

### Pastel

This is a cute and friendly color scheme, with light and soft colors

#### Hex Codes:

1. #E1F7D5
2. #FFBDBD
3. #C9C9FF
4. #FFFFFF
5. #F1CBFF
---

![vaporwave]({{< resource url="/images/vaporwave.svg" >}})

### Vaporwave

This is a color palette inspired by the Vaporwave movement, that aims to revitalize the from the 1980s and 1990s aesthetic.
#### Hex Codes:

1. #FF71CE
2. #01CDFE
3. #05FFA1
4. #B967FF
5. #FFFB96
---
