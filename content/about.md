---
title: "About"
date: 2021-10-13T15:52:41-03:00
draft: false
---

PaletteShowcase is a website designed to list color palettes to be used on your own design project. I hope you find what you are looking for!
